#include "KawaiiQPainterLayerImpl.hpp"

KawaiiQPainterLayerImpl::KawaiiQPainterLayerImpl(KawaiiQPainterLayer *model):
  KawaiiImageSourceImpl(model)
{
}

void KawaiiQPainterLayerImpl::directDraw()
{
  bool redrawNeeded = static_cast<KawaiiQPainterLayer*>(getModel())->isRedrawNeeded(painterCreater);
  if(redrawNeeded)
    refreshPixmaps();
  if(!isPixmapsCacheble() || !loadPixmaps())
    {
      QPainter *p = painterCreater();
      if(Q_LIKELY(p))
        {
          static_cast<KawaiiQPainterLayer*>(getModel())->getRedraw()(*p);
          p->end();
        }
    }
}
