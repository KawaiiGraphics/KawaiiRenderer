#ifndef KAWAIIGLPAINTEDLAYERIMPL_HPP
#define KAWAIIGLPAINTEDLAYERIMPL_HPP

#include <Kawaii3D/Renderpass/KawaiiGlPaintedLayer.hpp>
#include "KawaiiImageSourceImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiGlPaintedLayerImpl: virtual public KawaiiImageSourceImpl
{
public:
  KawaiiGlPaintedLayerImpl(KawaiiGlPaintedLayer *model);

  void directDraw() override final;

protected:
  std::function<QOpenGLContext *()> ctxGetter;

  virtual void refreshPixmaps() = 0;
};

#endif // KAWAIIGLPAINTEDLAYERIMPL_HPP
