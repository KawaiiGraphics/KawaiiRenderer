#include "KawaiiImageSourceImpl.hpp"

KawaiiImageSourceImpl::KawaiiImageSourceImpl(KawaiiImageSource *model):
  KawaiiRendererImpl(model),
  model(model),
  hasCache(false),
  pixmapsCacheble(false)
{
}

KawaiiImageSourceImpl::~KawaiiImageSourceImpl()
{
  if(hasCache)
    qCritical("KawaiiImageSourceImpl: detected memory leak! Ensure 'preDestruct' called in destructor!");
}

void KawaiiImageSourceImpl::draw()
{
  if(pixmapsCacheble && !model->isRedrawNeeded() && loadPixmaps())
    return;

  if(Q_UNLIKELY(!hasCache))
    {
      createCache();
      hasCache = true;
    }
  if(Q_UNLIKELY(model->isRefreshNeeded()))
    {
      updateCache();
      model->markRefreshed();
    }
  drawCache();
}

void KawaiiImageSourceImpl::validateCache()
{
  if(!hasCache)
    {
      createCache();
      hasCache = true;
    }
  if(model->isRefreshNeeded() || refreshNeeded)
    {
      updateCache();
      model->markRefreshed();
      refreshNeeded = false;
    }
}

bool KawaiiImageSourceImpl::isPixmapsCacheble() const
{
  return pixmapsCacheble;
}

void KawaiiImageSourceImpl::preDestruct()
{
  if(hasCache)
    {
      deleteCache();
      hasCache = false;
    }
}

void KawaiiImageSourceImpl::setPixmapsCacheble(bool newPixmapsCacheble)
{
  pixmapsCacheble = newPixmapsCacheble;
}
