#ifndef KAWAIIRENDERPASSIMPL_HPP
#define KAWAIIRENDERPASSIMPL_HPP

#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../KawaiiRenderer_global.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiRenderpassImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiRenderpassImpl(KawaiiRenderpass *model);
  ~KawaiiRenderpassImpl();

  void draw();
  size_t getCurrentSubpass() const;


protected:
  KawaiiRenderpass *getModel() const;

  bool getRenderingCacheble() const;
  void setRenderingCacheble(bool newRenderingCacheble);

  bool getCacheDirty() const;
  void setCacheDirty(bool newCacheDirty);

  void preDestruct();



  // IMPLEMENT
private:
  KawaiiRenderpass *model;
  size_t currentSubpass;

  bool renderingCacheble;
  bool cacheDirty;
  bool created;

  virtual void create() = 0;
  virtual void destroy() = 0;

  virtual bool start() = 0;
  virtual void nextSubpass() = 0;
  virtual void finish() = 0;
  virtual void drawCache() = 0;
};

#endif // KAWAIIRENDERPASSIMPL_HPP
