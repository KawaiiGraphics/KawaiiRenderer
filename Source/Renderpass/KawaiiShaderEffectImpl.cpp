#include "Shaders/KawaiiProgramImpl.hpp"
#include "KawaiiShaderEffectImpl.hpp"
#include "KawaiiGpuBufImpl.hpp"

KawaiiShaderEffectImpl::KawaiiShaderEffectImpl(KawaiiShaderEffect *model):
  KawaiiImageSourceImpl(model)
{
}

void KawaiiShaderEffectImpl::directDraw()
{
  if(auto effect = static_cast<KawaiiShaderEffect*>(getModel()); Q_LIKELY(effect->getEffect()))
    {
      auto progImpl = static_cast<KawaiiProgramImpl*>(effect->getEffect()->getRendererImpl());
      auto surfaceUBO = effect->getSurfaceUBO()?
            static_cast<KawaiiGpuBufImpl*>(effect->getSurfaceUBO()->getRendererImpl()):
            nullptr;
      auto cameraUBO = effect->getCamera()?
          static_cast<KawaiiGpuBufImpl*>(effect->getCamera()->getUniforms()->getRendererImpl()):
            nullptr;
      if(cameraUBO)
        cameraUBO->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getCameraUboLocation());
      if(surfaceUBO)
        surfaceUBO->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getSurfaceUboLocation());

      if(progImpl->use())
        drawQuad();
    }
}
