#ifndef KAWAIIIMAGESOURCEIMPL_HPP
#define KAWAIIIMAGESOURCEIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Renderpass/KawaiiImageSource.hpp>
#include "../KawaiiRenderer_global.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiImageSourceImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiImageSourceImpl(KawaiiImageSource *model);
  ~KawaiiImageSourceImpl();

  void draw();
  void validateCache();
  virtual void directDraw() = 0;

protected:
  inline KawaiiImageSource* getModel() const
  { return model; }

  bool isPixmapsCacheble() const;
  void setPixmapsCacheble(bool newPixmapsCacheble);
  void preDestruct();

  virtual void drawQuad() = 0;
  inline virtual bool loadPixmaps() { return false; }



  // IMPLEMENT
private:
  KawaiiImageSource *model;
  bool hasCache;
  bool pixmapsCacheble;
protected:
  bool refreshNeeded;

  virtual void createCache() = 0;
  virtual void drawCache() = 0;
  virtual void updateCache() = 0;
  virtual void deleteCache() = 0;
};

#endif // KAWAIIIMAGESOURCEIMPL_HPP
