#ifndef KAWAIISCENELAYERIMPL_HPP
#define KAWAIISCENELAYERIMPL_HPP

#include <Kawaii3D/Renderpass/KawaiiSceneLayer.hpp>
#include "KawaiiImageSourceImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiSceneLayerImpl: public virtual KawaiiImageSourceImpl
{
public:
  KawaiiSceneLayerImpl(KawaiiSceneLayer *model);

  // KawaiiImageSourceImpl interface
public:
  void directDraw() override final;
};

#endif // KAWAIISCENELAYERIMPL_HPP
