#ifndef KAWAIIQPAINTERLAYERIMPL_HPP
#define KAWAIIQPAINTERLAYERIMPL_HPP

#include <Kawaii3D/Renderpass/KawaiiQPainterLayer.hpp>
#include "KawaiiImageSourceImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiQPainterLayerImpl: virtual public KawaiiImageSourceImpl
{
public:
  KawaiiQPainterLayerImpl(KawaiiQPainterLayer *model);

  void directDraw() override final;

protected:
  std::function<QPainter*()> painterCreater;

  virtual void refreshPixmaps() = 0;
};

#endif // KAWAIIQPAINTERLAYERIMPL_HPP
