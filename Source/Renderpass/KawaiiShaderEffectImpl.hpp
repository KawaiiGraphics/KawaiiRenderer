#ifndef KAWAIISHADEREFFECTIMPL_HPP
#define KAWAIISHADEREFFECTIMPL_HPP

#include <Kawaii3D/Renderpass/KawaiiShaderEffect.hpp>
#include "KawaiiImageSourceImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiShaderEffectImpl: virtual public KawaiiImageSourceImpl
{
public:
  KawaiiShaderEffectImpl(KawaiiShaderEffect *model);

  void directDraw() override final;
};

#endif // KAWAIISHADEREFFECTIMPL_HPP
