#include "KawaiiSceneLayerImpl.hpp"
#include "KawaiiCameraImpl.hpp"
#include "KawaiiGpuBufImpl.hpp"

KawaiiSceneLayerImpl::KawaiiSceneLayerImpl(KawaiiSceneLayer *model):
  KawaiiImageSourceImpl(model)
{
}

void KawaiiSceneLayerImpl::directDraw()
{
  if(auto sceneLayer = static_cast<KawaiiSceneLayer*>(getModel()); sceneLayer->getCamera())
    {
      auto cam = static_cast<KawaiiCameraImpl*>(sceneLayer->getCamera()->getRendererImpl());
      auto bufImpl = static_cast<KawaiiGpuBufImpl*>(sceneLayer->getSurfaceUBO()->getRendererImpl());
      if(cam && bufImpl)
        cam->draw(bufImpl);
    }
}
