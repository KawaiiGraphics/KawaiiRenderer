#include "KawaiiRenderpassImpl.hpp"
#include "KawaiiImageSourceImpl.hpp"

KawaiiRenderpassImpl::KawaiiRenderpassImpl(KawaiiRenderpass *model):
  KawaiiRendererImpl(model),
  model(model),
  currentSubpass(0),
  renderingCacheble(false),
  cacheDirty(true),
  created(false)
{
}

KawaiiRenderpassImpl::~KawaiiRenderpassImpl()
{
  if(created)
    qCritical("KawaiiRenderpassImpl: detected memory leak! Ensure 'preDestruct' called in destructor!");
}

void KawaiiRenderpassImpl::draw()
{
  if(Q_UNLIKELY(getModel()->isDirty() && created))
    {
      destroy();
      created = false;
      cacheDirty = true;
    }

  if(Q_UNLIKELY(!created))
    {
      create();
      getModel()->markUpdated();
      created = true;
    }

  if(renderingCacheble)
    {
      model->forallSubpasses([] (const KawaiiRenderpass::Subpass &subpass) {
          if(subpass.imgSrc)
            if(auto imgSrc = static_cast<KawaiiImageSourceImpl*>(subpass.imgSrc->getRendererImpl()); imgSrc)
              imgSrc->validateCache();
        });
      if(cacheDirty)
        {
          start();
          model->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
              if(subpass.imgSrc)
                if(auto imgSrc = static_cast<KawaiiImageSourceImpl*>(subpass.imgSrc->getRendererImpl()); imgSrc)
                  imgSrc->draw();
              if(currentSubpass < model->subpassCount() - 1)
                {
                  ++currentSubpass;
                  nextSubpass();
                }
            });
          finish();
          cacheDirty = false;
        }
      drawCache();
    } else
    {
      if(start())
        {
          model->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
              if(subpass.imgSrc)
                if(auto imgSrc = static_cast<KawaiiImageSourceImpl*>(subpass.imgSrc->getRendererImpl()); imgSrc)
                  imgSrc->directDraw();
              if(currentSubpass < model->subpassCount() - 1)
                {
                  ++currentSubpass;
                  nextSubpass();
                }
            });
          finish();
        }
    }
  currentSubpass = 0;
}

bool KawaiiRenderpassImpl::getRenderingCacheble() const
{
  return renderingCacheble;
}

void KawaiiRenderpassImpl::setRenderingCacheble(bool newRenderingCacheble)
{
  renderingCacheble = newRenderingCacheble;
}

bool KawaiiRenderpassImpl::getCacheDirty() const
{
  return cacheDirty;
}

void KawaiiRenderpassImpl::setCacheDirty(bool newCacheDirty)
{
  cacheDirty = newCacheDirty;
}

void KawaiiRenderpassImpl::preDestruct()
{
  if(created)
    {
      destroy();
      created = false;
    }
}

size_t KawaiiRenderpassImpl::getCurrentSubpass() const
{
  return currentSubpass;
}

KawaiiRenderpass *KawaiiRenderpassImpl::getModel() const
{
  return model;
}
