#include "KawaiiGlPaintedLayerImpl.hpp"

KawaiiGlPaintedLayerImpl::KawaiiGlPaintedLayerImpl(KawaiiGlPaintedLayer *model):
  KawaiiImageSourceImpl(model)
{
}

void KawaiiGlPaintedLayerImpl::directDraw()
{
  if(!static_cast<KawaiiGlPaintedLayer*>(getModel())->getRedraw()) return;

  bool redrawNeeded = static_cast<KawaiiGlPaintedLayer*>(getModel())->isRedrawNeeded(ctxGetter);
  if(redrawNeeded)
    refreshPixmaps();
  if(!isPixmapsCacheble() || !loadPixmaps())
    static_cast<KawaiiGlPaintedLayer*>(getModel())->getRedraw()(ctxGetter());
}
