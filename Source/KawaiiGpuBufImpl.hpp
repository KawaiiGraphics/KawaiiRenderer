#ifndef KAWAIIGPUBUFIMPL_HPP
#define KAWAIIGPUBUFIMPL_HPP

#include "KawaiiBufferHandle.hpp"
#include <Kawaii3D/KawaiiGpuBuf.hpp>

class KawaiiRootImpl;
class KawaiiTextureImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiGpuBufImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  KawaiiGpuBufImpl(KawaiiGpuBuf *model);
  ~KawaiiGpuBufImpl();

  void bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint);
  void userBinding(KawaiiBufferTarget target, uint32_t bindingPoint);
  void unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint);

  KawaiiGpuBuf* getModel() const;

signals:
  void handleChanged();

protected:
  std::function<void()> onRootChanged;

  inline KawaiiRootImpl* getRoot() const
  { return root; }

  inline KawaiiBufferHandle* getHandle() const
  { return d; }



  //IMPLEMENT
private:
  KawaiiGpuBuf *model;
  KawaiiRootImpl *root;

  KawaiiBufferHandle *d;

  void onRelocated(const void *data, size_t size);
  void onDataChanged(size_t offset, size_t bytes);

  void onParentChanged();
  void onHandleInvalidated();

  void preDestruct();
};

#endif // KAWAIIGPUBUFIMPL_HPP
