#ifndef KAWAIIPROGRAMIMPL_HPP
#define KAWAIIPROGRAMIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>

#include "../KawaiiRenderer_global.hpp"
#include <QPointer>

class KawaiiRootImpl;
class KawaiiShaderImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiProgramImpl : public KawaiiRendererImpl
{
  Q_OBJECT
  friend class KawaiiShaderImpl;
public:
  explicit KawaiiProgramImpl(KawaiiProgram *model);
  ~KawaiiProgramImpl();

  virtual bool use() const = 0;

protected:
  inline KawaiiRootImpl *getRoot() const
  { return root; }

  inline void preDestruct()
  {
    if(ready)
      {
        deleteProgram();
        ready = false;
      }
  }

  void forallShaders(const std::function<void(KawaiiShaderImpl*)> &func) const;



  //IMPLEMENT
private:
  KawaiiProgram *model;
  KawaiiRootImpl *root;

  bool ready;

  virtual void createProgram() = 0;
  virtual void deleteProgram() = 0;

  virtual void reAddShader(KawaiiShaderImpl *shader) = 0;

  void onParentChanged();

  // KawaiiRendererImpl interface
  void initConnections() override;
};

#endif // KAWAIIPROGRAMIMPL_HPP
