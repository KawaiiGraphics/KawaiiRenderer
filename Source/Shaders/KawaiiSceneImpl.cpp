#include "KawaiiSceneImpl.hpp"

#include "KawaiiRootImpl.hpp"
#include "KawaiiMaterialImpl.hpp"
#include "KawaiiProgramImpl.hpp"
#include "Geometry/KawaiiMeshInstanceImpl.hpp"

#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

KawaiiSceneImpl::KawaiiSceneImpl(KawaiiScene *model):
  KawaiiRendererImpl(model),
  model(model)
{
  if(!model)
    deleteLater();
}

KawaiiSceneImpl::~KawaiiSceneImpl()
{
}

void KawaiiSceneImpl::draw(KawaiiGpuBufImpl *cameraUBO, KawaiiGpuBufImpl *surfaceUBO)
{
  //Bind ubo
  if(cameraUBO)
    cameraUBO->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getCameraUboLocation());
  if(surfaceUBO)
    surfaceUBO->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getSurfaceUboLocation());

  for(auto i = model->getScene().constBegin(); i != model->getScene().constEnd(); ++i)
    if(i->shaderProg && !i->instances.isEmpty())
      {
        auto progImpl = static_cast<KawaiiProgramImpl*>(i->shaderProg->getRendererImpl());
        if(!progImpl)
          continue;

        //Use shader program
        if(!progImpl->use())
          continue;

        //Bind material
        if(i.key())
          if(auto materialImpl = static_cast<KawaiiMaterialImpl*>(i.key()->getRendererImpl()); materialImpl)
            materialImpl->bind();

        //Draw
        for(auto mesh: i->instances)
          if(auto meshImpl = static_cast<KawaiiMeshInstanceImpl*>(mesh->getRendererImpl()); meshImpl)
            meshImpl->draw();
      }
}
