#ifndef KAWAIISHADERIMPL_HPP
#define KAWAIISHADERIMPL_HPP

#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <QPointer>

#include "../KawaiiRenderer_global.hpp"

class KawaiiRootImpl;
class KawaiiProgramImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiShaderImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  explicit KawaiiShaderImpl(KawaiiShader *model);
  ~KawaiiShaderImpl();

protected:
  inline void preDestruct()
  {
    if(ready)
      {
        deleteShader();
        ready = false;
      }
  }

  inline KawaiiRootImpl* getRoot() const
  { return root; }

  ///Expected to be called in derived constructor
  inline void initResource()
  {
    if(!ready)
      {
        createShader();
        ready = true;
      }
  }



  //IMPLEMENT
private:
  KawaiiShader *model;
  KawaiiRootImpl *root;
  KawaiiProgramImpl *prog;

  bool ready;

  virtual void createShader() = 0;
  virtual void deleteShader() = 0;

  virtual void recompile() = 0;

  void reparsed(KawaiiShader *shader);

  void updateProgramOwner();
  void onParentChanged();

  // KawaiiRendererImpl interface
  void initConnections() override;
};

#endif // KAWAIISHADERIMPL_HPP
