#ifndef KAWAIISCENEIMPL_HPP
#define KAWAIISCENEIMPL_HPP

#include <Kawaii3D/Shaders/KawaiiScene.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "../KawaiiGpuBufImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiSceneImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  KawaiiSceneImpl(KawaiiScene *model);
  ~KawaiiSceneImpl();

  void draw(KawaiiGpuBufImpl *cameraUBO, KawaiiGpuBufImpl *surfaceUBO);



  //IMPLEMENT
private:
  KawaiiScene *model;
};

#endif // KAWAIISCENEIMPL_HPP
