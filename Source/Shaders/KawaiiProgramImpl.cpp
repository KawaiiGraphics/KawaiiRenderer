#include "KawaiiProgramImpl.hpp"

#include "KawaiiShaderImpl.hpp"
#include "KawaiiRootImpl.hpp"

#include <Kawaii3D/Shaders/KawaiiProgram.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

KawaiiProgramImpl::KawaiiProgramImpl(KawaiiProgram *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  ready(false)
{
  if(model)
    onParentChanged();
  else
    deleteLater();
}

KawaiiProgramImpl::~KawaiiProgramImpl()
{
  Q_ASSERT(!ready);
}

void KawaiiProgramImpl::forallShaders(const std::function<void (KawaiiShaderImpl*)> &func) const
{
  model->forallModules([&func] (KawaiiShader *shader) {
    if(shader->getRendererImpl())
      func(static_cast<KawaiiShaderImpl*>(shader->getRendererImpl()));
    });
}

void KawaiiProgramImpl::onParentChanged()
{
  if(!ready)
    {
      root = KawaiiRootImpl::findRootImpl(this);
      return;
    }

  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      deleteProgram();
      root = _root;
      createProgram();
    }
}

void KawaiiProgramImpl::initConnections()
{
  if(!ready)
    {
      createProgram();
      ready = true;
    }
}
