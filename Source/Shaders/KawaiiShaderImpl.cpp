﻿#include "KawaiiShaderImpl.hpp"

#include "KawaiiProgramImpl.hpp"
#include "KawaiiRootImpl.hpp"
#include "KawaiiProgramImpl.hpp"

#include <QRegularExpression>
#include <sib_utils/Strings.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <Kawaii3D/Shaders/KawaiiProgram.hpp>

KawaiiShaderImpl::KawaiiShaderImpl(KawaiiShader *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  prog(nullptr),
  ready(false)
{
  if(model)
    {
      connect(model, &KawaiiShader::reparsed, this, &KawaiiShaderImpl::reparsed);
      connect(model, &KawaiiShader::parentUpdated, this, &KawaiiShaderImpl::onParentChanged);

      onParentChanged();
    } else
    deleteLater();
}

KawaiiShaderImpl::~KawaiiShaderImpl()
{
  Q_ASSERT(!ready);
}

void KawaiiShaderImpl::reparsed(KawaiiShader *shader)
{
  if(shader != model)
    return;

  recompile();
  if(prog)
    prog->reAddShader(this);
}

void KawaiiShaderImpl::updateProgramOwner()
{
  if(model->getProgram())
    prog = static_cast<KawaiiProgramImpl*>(model->getProgram()->getRendererImpl());
  else
    prog = nullptr;
}

void KawaiiShaderImpl::onParentChanged()
{
  updateProgramOwner();

  if(!ready)
    {
      root = KawaiiRootImpl::findRootImpl(this);
      return;
    }

  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      deleteShader();
      root = _root;
      createShader();
    }
}

void KawaiiShaderImpl::initConnections()
{
  updateProgramOwner();
  Q_ASSERT(ready);
}
