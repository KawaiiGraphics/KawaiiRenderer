#include "Shaders/KawaiiSceneImpl.hpp"
#include "KawaiiCameraImpl.hpp"
#include "KawaiiRootImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include <QEvent>

KawaiiCameraImpl::KawaiiCameraImpl(KawaiiCamera *model):
  KawaiiRendererImpl(model),
  model(model)
{
}

KawaiiCameraImpl::~KawaiiCameraImpl()
{
}

KawaiiCamera *KawaiiCameraImpl::getModel() const
{
  return model;
}

glm::mat4 KawaiiCameraImpl::getViewProjectionMat(const glm::mat4 &projMat) const
{
  return model->getViewProjectionMat(projMat);
}

void KawaiiCameraImpl::draw(KawaiiGpuBufImpl *sfcUbo)
{
  if(scene() && ubo())
    scene()->draw(ubo(), sfcUbo);
}

KawaiiGpuBufImpl *KawaiiCameraImpl::ubo() const
{
  return static_cast<KawaiiGpuBufImpl*>(model->getUniforms()->getRendererImpl());
}

KawaiiSceneImpl *KawaiiCameraImpl::scene() const
{
  if(model->getScene())
    return static_cast<KawaiiSceneImpl*>(model->getScene()->getRendererImpl());
  else
    return nullptr;
}
