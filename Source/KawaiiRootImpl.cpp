#include "KawaiiRootImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>

KawaiiRootImpl::KawaiiRootImpl(KawaiiRoot *model):
  KawaiiRendererImpl(model)
{
}

KawaiiRootImpl::~KawaiiRootImpl()
{
}

KawaiiRootImpl *KawaiiRootImpl::findRootImpl(KawaiiRendererImpl *rnd)
{
  auto root_data = KawaiiRoot::getRoot(rnd->getData());
  if(!root_data)
    return nullptr;

  return static_cast<KawaiiRootImpl*>(root_data->getRendererImpl());
}

KawaiiRootImpl::OffscreenRenderIterator KawaiiRootImpl::addOffscreenRender(std::function<void ()> &&hook, KawaiiFramebuffer *fbo)
{
  const auto i = offscreenRenderHooks.insert(offscreenRenderHooks.end(), {std::move(hook), fbo});
  return {i, this};
}

KawaiiRootImpl::OffscreenRenderIterator KawaiiRootImpl::addOffscreenRender(std::function<void ()> &hook, KawaiiFramebuffer *fbo)
{
  const auto i = offscreenRenderHooks.insert(offscreenRenderHooks.end(), {hook, fbo});
  return {i, this};
}

void KawaiiRootImpl::removeOffscreenRender(const KawaiiRootImpl::OffscreenRenderIterator &i)
{
  if(i.root == this)
    offscreenRenderHooks.erase(i.i);
}

void KawaiiRootImpl::drawOffscreen()
{
  if(!offscreenRenderHooks.empty())
    {
      beginOffscreen();
      for(const auto &i: offscreenRenderHooks)
        i.first();
      for(const auto &i: offscreenRenderHooks)
        if(auto *rp = i.second? i.second->getRenderpass(): nullptr; rp)
          rp->markRedrawn();
      endOffscreen();
    }
}
