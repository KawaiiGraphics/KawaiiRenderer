#ifndef KAWAIIBUFBINDINGIMPL_HPP
#define KAWAIIBUFBINDINGIMPL_HPP

#include <Kawaii3D/KawaiiBufBinding.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "KawaiiGpuBufImpl.hpp"

//todo: on invalidate buffer
class KAWAIIRENDERER_SHARED_EXPORT KawaiiBufBindingImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiBufBindingImpl(KawaiiBufBinding *model);
  ~KawaiiBufBindingImpl();



  //IMPLEMENT
private:
  KawaiiGpuBufImpl *buf;
  KawaiiBufBinding *model;

  uint32_t bindingPoint;

  void bind();
  void onBindingPointChanged(uint32_t binding);
  void onBufferChanged(KawaiiGpuBuf *target);

  // KawaiiRendererImpl interface
  void initConnections() override;
};

#endif // KAWAIIBUFBINDINGIMPL_HPP
