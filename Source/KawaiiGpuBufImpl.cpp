#include "KawaiiGpuBufImpl.hpp"

#include "KawaiiRootImpl.hpp"
#include <Kawaii3D/KawaiiGpuBuf.hpp>

KawaiiGpuBufImpl::KawaiiGpuBufImpl(KawaiiGpuBuf *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  d(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiGpuBuf::relocated, this, &KawaiiGpuBufImpl::onRelocated);
      connect(model, &KawaiiGpuBuf::dataChanged, this, &KawaiiGpuBufImpl::onDataChanged);
      connect(model, &KawaiiGpuBuf::parentUpdated, this, &KawaiiGpuBufImpl::onParentChanged);

      onParentChanged();
    } else
    deleteLater();
}

KawaiiGpuBufImpl::~KawaiiGpuBufImpl()
{
  if(d)
    delete d;
}

void KawaiiGpuBufImpl::bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  if(d)
    d->bindToBlock(target, bindingPoint);
}

void KawaiiGpuBufImpl::userBinding(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  if(d)
    d->userBinding(target, bindingPoint);
}

void KawaiiGpuBufImpl::unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  if(d)
    d->unbindFromBlock(target, bindingPoint);
}

KawaiiGpuBuf *KawaiiGpuBufImpl::getModel() const
{
  return model;
}

void KawaiiGpuBufImpl::onRelocated(const void *data, size_t size)
{
  if(d)
    d->setBufferData(data, size);
}

void KawaiiGpuBufImpl::onDataChanged(size_t offset, size_t bytes)
{
  if(d && offset + bytes <= model->getDataSize())
    d->sendChunkToGpu(offset, bytes);
}

void KawaiiGpuBufImpl::onParentChanged()
{
  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      root = _root;
      if(onRootChanged)
        onRootChanged();

      auto new_handle = root? root->createBuffer(model->getData(), model->getDataSize(), {KawaiiBufferTarget::UBO, KawaiiBufferTarget::SSBO}): nullptr;
      KawaiiBufferHandle::replace(d, new_handle, std::bind(&KawaiiGpuBufImpl::onHandleInvalidated, this));
      emit handleChanged();
    }
}

void KawaiiGpuBufImpl::onHandleInvalidated()
{
  auto new_handle = root? root->createBuffer(model->getData(), model->getDataSize(), {KawaiiBufferTarget::UBO, KawaiiBufferTarget::SSBO}): nullptr;
  KawaiiBufferHandle::replace(d, new_handle, std::bind(&KawaiiGpuBufImpl::onHandleInvalidated, this));
  emit handleChanged();
}
