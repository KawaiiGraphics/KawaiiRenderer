#include "KawaiiMeshInstanceImpl.hpp"

#include "KawaiiMeshImpl.hpp"
#include "KawaiiRootImpl.hpp"

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>
#include <memory>

KawaiiMeshInstanceImpl::KawaiiMeshInstanceImpl(KawaiiMeshInstance *model):
  KawaiiRendererImpl(model),
  model(model)
{
  if(!model)
    deleteLater();
}

KawaiiMeshInstanceImpl::~KawaiiMeshInstanceImpl()
{
}

void KawaiiMeshInstanceImpl::draw()
{
  if(!model->getInstanceCount()) return;

  bindUbo();

  switch (model->getInstanceCount())
    {
    case 1:
      getMesh()->draw();
      break;

    default:
      getMesh()->drawInstanced(model->getInstanceCount());
      break;
    }
}

void KawaiiMeshInstanceImpl::bindUbo() const
{
  if(auto ubo = getUniforms(); ubo)
    ubo->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getModelUboLocation());
}

size_t KawaiiMeshInstanceImpl::getIndexCount() const
{
  return model->getMesh()? model->getMesh()->trianglesCount() * 3: 0;
}

KawaiiMeshImpl *KawaiiMeshInstanceImpl::getMesh() const
{
  if(model->getMesh())
    return static_cast<KawaiiMeshImpl*>(model->getMesh()->getRendererImpl());
  else
    return nullptr;
}

KawaiiGpuBufImpl *KawaiiMeshInstanceImpl::getUniforms() const
{
  if(model->getUniforms())
    return static_cast<KawaiiGpuBufImpl*>(model->getUniforms()->getRendererImpl());
  else
    return nullptr;
}
