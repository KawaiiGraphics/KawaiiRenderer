#ifndef KAWAIIMESHIMPL_HPP
#define KAWAIIMESHIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Geometry/KawaiiMesh3D.hpp>

#include "../KawaiiRenderer_global.hpp"
#include "../KawaiiBufferHandle.hpp"

class KawaiiRootImpl;

//todo: on invalidate buffers
class KAWAIIRENDERER_SHARED_EXPORT KawaiiMeshImpl : public KawaiiRendererImpl
{
  Q_OBJECT

public:
  explicit KawaiiMeshImpl(KawaiiMesh3D *model);
  ~KawaiiMeshImpl();

  virtual void draw() const = 0;
  virtual void drawInstanced(size_t instances) const = 0;



protected:
  KawaiiBufferHandle *getBaseAttrBuf() const;
  KawaiiBufferHandle *getIndicesBuffer() const;

  inline uint32_t getMinIndex() const
  { return minIndex; }

  inline uint32_t getMaxIndex() const
  { return maxIndex; }

  inline void preDestruct()
  {
    if(ready)
      {
        deleteVertexArray();
        ready = false;
      }
  }

  inline KawaiiRootImpl* getRoot() const
  { return root; }

  KawaiiMesh3D *getModel() const
  { return model; }



  //IMPLEMENT
private:
  KawaiiMesh3D *model;
  KawaiiRootImpl *root;

  KawaiiBufferHandle *baseAttr;
  KawaiiBufferHandle *indices;

  uint32_t minIndex;
  uint32_t maxIndex;

  bool ready;

  virtual void createVertexArray() = 0;
  virtual void bindBaseAttr() = 0;
  virtual void bindIndices() = 0;
  virtual void deleteVertexArray() = 0;

  void verticesRelocated();
  void verticesUpdated(size_t index, size_t count);

  void triangleAdded(const KawaiiTriangle3D::Instance &tr);
  void trianglesRemoved(size_t i, size_t count);

  void trianglesRelocated();
  void indexUpdated(int i);

  void onParentChanged();

  void onBaseAttrBufInvalidated();
  void onIndicesBufInvalidated();

  // KawaiiRendererImpl interface
private:
  void calculateMinMaxIndices();
  void initConnections() override;
};

#endif // KAWAIIMESHIMPL_HPP
