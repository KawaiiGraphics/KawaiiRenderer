#ifndef KAWAIIMESHINSTANCEIMPL_HPP
#define KAWAIIMESHINSTANCEIMPL_HPP

#include <Kawaii3D/Geometry/KawaiiMeshInstance.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>

#include "../KawaiiGpuBufImpl.hpp"

class KawaiiMeshImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiMeshInstanceImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiMeshInstanceImpl(KawaiiMeshInstance *model);
  ~KawaiiMeshInstanceImpl();

  virtual void draw();

protected:
  void bindUbo() const;
  size_t getIndexCount() const;
  KawaiiMeshImpl *getMesh() const;



  //IMPLEMENT
private:
  KawaiiMeshInstance *model;

  KawaiiGpuBufImpl *getUniforms() const;
};

#endif // KAWAIIMESHINSTANCEIMPL_HPP
