#include "KawaiiMeshImpl.hpp"

#include "KawaiiRootImpl.hpp"
#include "KawaiiMeshInstanceImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <QChildEvent>
#include <algorithm>

KawaiiMeshImpl::KawaiiMeshImpl(KawaiiMesh3D *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  baseAttr(nullptr),
  indices(nullptr),
  minIndex(0),
  maxIndex(std::numeric_limits<uint32_t>::max()),
  ready(false)
{
  if(model)
    {
      connect(model, &KawaiiMesh3D::vertexAdded,      this, &KawaiiMeshImpl::verticesRelocated);
      connect(model, &KawaiiMesh3D::verticesRemoved,  this, &KawaiiMeshImpl::verticesRelocated);
      connect(model, &KawaiiMesh3D::verticesUpdated,  this, &KawaiiMeshImpl::verticesUpdated);
      connect(model, &KawaiiMesh3D::triangleAdded,    this, &KawaiiMeshImpl::trianglesRelocated);
      connect(model, &KawaiiMesh3D::trianglesRemoved, this, &KawaiiMeshImpl::trianglesRelocated);
      connect(model, &KawaiiMesh3D::indicesUpdated,   this, &KawaiiMeshImpl::indexUpdated);
      connect(model, &KawaiiMesh3D::parentUpdated,    this, &KawaiiMeshImpl::onParentChanged);

      onParentChanged();
    }
}

KawaiiMeshImpl::~KawaiiMeshImpl()
{
  Q_ASSERT(!ready);

  if(baseAttr)
    delete baseAttr;

  if(indices)
    delete indices;
}

KawaiiBufferHandle *KawaiiMeshImpl::getBaseAttrBuf() const
{
  return baseAttr;
}

KawaiiBufferHandle *KawaiiMeshImpl::getIndicesBuffer() const
{
  return indices;
}

void KawaiiMeshImpl::verticesRelocated()
{
  baseAttr->setData(model->getVertices());
}

void KawaiiMeshImpl::verticesUpdated(size_t index, size_t count)
{
  baseAttr->updateDataElements<KawaiiPoint3D>(index, count);
}

namespace {
  template<typename T>
  std::pair<T, T> minmax_element(const std::initializer_list<T> &lst)
  {
    const auto minmax = std::minmax_element(lst.begin(), lst.end());
    return {*minmax.first, *minmax.second};
  }
}

void KawaiiMeshImpl::triangleAdded(const KawaiiTriangle3D::Instance &tr)
{
  std::tie(minIndex, maxIndex) = minmax_element({minIndex, maxIndex, tr.getFirstIndex(), tr.getSecondIndex(), tr.getThirdIndex()});
}

void KawaiiMeshImpl::trianglesRemoved(size_t i, size_t count)
{
  const uint32_t *b = reinterpret_cast<const uint32_t*>(model->getRawTriangles().data() + i),
      *e = b + 3 * count;

  const auto minmax = std::minmax_element(b, e);
  if(minIndex == *minmax.first || maxIndex == *minmax.second)
    calculateMinMaxIndices();
}

void KawaiiMeshImpl::trianglesRelocated()
{
  indices->setData(model->getRawTriangles());
}

void KawaiiMeshImpl::indexUpdated(int i)
{
  indices->updateDataElements<KawaiiTriangle3D>(i, 1);
}

void KawaiiMeshImpl::onParentChanged()
{
  if(!ready)
    {
      root = KawaiiRootImpl::findRootImpl(this);
      return;
    }

  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      deleteVertexArray();
      ready = false;
      root = _root;

      auto new_indices = root? root->createBuffer(model->getRawTriangles(), {KawaiiBufferTarget::ElementArray}): nullptr;
      KawaiiBufferHandle::replace(indices, new_indices, std::bind(&KawaiiMeshImpl::onIndicesBufInvalidated, this));

      auto new_baseAttr = root? root->createBuffer(model->getVertices(), {KawaiiBufferTarget::VertexArray}): nullptr;
      KawaiiBufferHandle::replace(baseAttr, new_baseAttr, std::bind(&KawaiiMeshImpl::onBaseAttrBufInvalidated, this));

      createVertexArray();
      ready = true;
    }
}

void KawaiiMeshImpl::onBaseAttrBufInvalidated()
{
  auto new_handle = root? root->createBuffer(model->getVertices(), {KawaiiBufferTarget::VertexArray}): nullptr;
  KawaiiBufferHandle::replace(baseAttr, new_handle, std::bind(&KawaiiMeshImpl::onBaseAttrBufInvalidated, this));
  if(ready)
    bindBaseAttr();
}

void KawaiiMeshImpl::onIndicesBufInvalidated()
{
  auto new_handle = root? root->createBuffer(model->getRawTriangles(), {KawaiiBufferTarget::ElementArray}): nullptr;
  KawaiiBufferHandle::replace(indices, new_handle, std::bind(&KawaiiMeshImpl::onIndicesBufInvalidated, this));
  if(ready)
    bindIndices();
}

void KawaiiMeshImpl::calculateMinMaxIndices()
{
  if(model->getRawTriangles().empty())
    {
      minIndex = maxIndex = 0;
      return;
    }

  const uint32_t *b = reinterpret_cast<const uint32_t*>(model->getRawTriangles().data()),
      *e = b + model->getRawTriangles().size();

  const auto min_max = std::minmax_element(b, e);
  minIndex  = *min_max.first;
  maxIndex = *min_max.second;
}

void KawaiiMeshImpl::initConnections()
{
  if(!ready)
    {
      baseAttr = root->createBuffer(model->getVertices(), {KawaiiBufferTarget::VertexArray});
      indices = root->createBuffer(model->getRawTriangles(), {KawaiiBufferTarget::ElementArray});

      baseAttr->setOnInvalidate(std::bind(&KawaiiMeshImpl::onBaseAttrBufInvalidated, this));
      indices->setOnInvalidate(std::bind(&KawaiiMeshImpl::onIndicesBufInvalidated, this));

      calculateMinMaxIndices();

      createVertexArray();
      ready = true;
    }
}
