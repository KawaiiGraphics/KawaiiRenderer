#ifndef KAWAIIROOTIMPL_HPP
#define KAWAIIROOTIMPL_HPP

#include <Kawaii3D/KawaiiRoot.hpp>
#include <functional>
#include <QWindow>
#include <vector>
#include <list>

#include "KawaiiBufferHandle.hpp"
#include "Textures/KawaiiTextureHandle.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiRootImpl : public KawaiiRendererImpl
{
  Q_OBJECT

public:
  struct OffscreenRenderIterator {
    std::list<std::pair<std::function<void()>, KawaiiFramebuffer*>>::iterator i;
    KawaiiRootImpl *root;
  };

  explicit KawaiiRootImpl(KawaiiRoot *model);
  virtual ~KawaiiRootImpl();

  static KawaiiRootImpl* findRootImpl(KawaiiRendererImpl *rnd);

  virtual KawaiiBufferHandle *createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets) = 0;

  virtual KawaiiTextureHandle *createTexture(KawaiiTexture *model) = 0;

  template<typename T>
  inline KawaiiBufferHandle *createBuffer(const std::vector<T> &data, const std::initializer_list<KawaiiBufferTarget> &availableTargets)
  {
    return createBuffer(data.data(), sizeof(T) * data.size(), availableTargets);
  }

  ///Returns iteraror to added hook
  OffscreenRenderIterator addOffscreenRender(std::function<void()> &&hook, KawaiiFramebuffer *fbo);

  ///Returns iteraror to added hook
  OffscreenRenderIterator addOffscreenRender(std::function<void()> &hook, KawaiiFramebuffer *fbo);

  void removeOffscreenRender(const OffscreenRenderIterator &i);

  void drawOffscreen();



  //IMPLEMENT
private:
  std::list<std::pair<std::function<void()>, KawaiiFramebuffer*>> offscreenRenderHooks;

  virtual void beginOffscreen() = 0;
  virtual void endOffscreen() = 0;
};

#endif // KAWAIIROOTIMPL_HPP
