#include "KawaiiMaterialImpl.hpp"
#include "KawaiiRootImpl.hpp"

#include <Kawaii3D/KawaiiGpuBuf.hpp>
#include <Kawaii3D/KawaiiMaterial.hpp>
#include <Kawaii3D/KawaiiRoot.hpp>

KawaiiMaterialImpl::KawaiiMaterialImpl(KawaiiMaterial *model):
  KawaiiRendererImpl(model),
  model(model),
  ubo(nullptr)
{
  if(!model)
    deleteLater();
}

KawaiiMaterialImpl::~KawaiiMaterialImpl()
{
}

void KawaiiMaterialImpl::bind() const
{
  if(model->getUniforms())
    {
      auto *ubo = static_cast<KawaiiGpuBufImpl*>(model->getUniforms()->getRendererImpl());
      ubo->bindToBlock(KawaiiBufferTarget::UBO, KawaiiShader::getMaterialUboLocation());
    }
}
