#include "KawaiiBufferHandle.hpp"
#include <QMetaObject>

void KawaiiBufferHandle::setOnInvalidate(const std::function<void ()> &func)
{
  onInvalidate = func;
}

void KawaiiBufferHandle::replace(std::unique_ptr<KawaiiBufferHandle> &oldHandle, KawaiiBufferHandle *newHandle, const std::function<void ()> &onInvalidate)
{
  if(newHandle)
    newHandle->setOnInvalidate(oldHandle? oldHandle->onInvalidate: onInvalidate);
  oldHandle.reset(newHandle);
}

void KawaiiBufferHandle::replace(KawaiiBufferHandle *&oldHandle, KawaiiBufferHandle *newHandle, const std::function<void()> &onInvalidate)
{
  if(newHandle)
    {
      if(oldHandle)
        {
          newHandle->setOnInvalidate(oldHandle->onInvalidate);
          delete oldHandle;
        } else
        newHandle->setOnInvalidate(onInvalidate);
    }
  oldHandle = newHandle;
}

void KawaiiBufferHandle::invalidate()
{
  if(onInvalidate)
    onInvalidate();
}

