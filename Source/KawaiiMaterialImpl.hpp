#ifndef KAWAIIMATERIALIMPL_HPP
#define KAWAIIMATERIALIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include "KawaiiGpuBufImpl.hpp"

class KawaiiMaterial;
class KawaiiRootImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiMaterialImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  KawaiiMaterialImpl(KawaiiMaterial *model);
  ~KawaiiMaterialImpl();

  void bind() const;



  //IMPLEMENT
private:
  KawaiiMaterial *model;
  KawaiiGpuBufImpl *ubo;
};

#endif // KAWAIIMATERIALIMPL_HPP
