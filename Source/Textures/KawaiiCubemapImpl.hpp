#ifndef KAWAIICUBEMAPIMPL_HPP
#define KAWAIICUBEMAPIMPL_HPP

#include "KawaiiTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiCubemap.hpp>
#include <array>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiCubemapImpl : public KawaiiTextureImpl
{
  Q_OBJECT

public:
  KawaiiCubemapImpl(KawaiiCubemap *model);



private:
  KawaiiCubemap *model;
  std::array<std::unique_ptr<QImage>, 6> rgbImg;
  QSize sz;
  std::vector<std::byte> bytes;
  size_t bytesPerPixel;
  KawaiiTextureFormat lastTextureFmt;


  void updateImg(quint16 index);

  KawaiiTextureFormat checkTextureFormat();
  void convertAllSidesToRgba(const QSize &sz);

  void onHandleInvalidated();

  const QImage& getImg(uint16_t i) const;
};

#endif // KAWAIICUBEMAPIMPL_HPP
