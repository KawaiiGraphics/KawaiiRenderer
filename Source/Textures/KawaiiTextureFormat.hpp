#ifndef KAWAIITEXTUREFORMAT_HPP
#define KAWAIITEXTUREFORMAT_HPP

#include <cstdint>

enum class KawaiiTextureFormat: uint8_t
{
  Red,
  RedGreen,
  RedGreenBlue,
  RedGreenBlueAlpha,

  BlueGreenRed,
  BlueGreenRedAlpha,

  Depth,
  DepthStencil
};

#endif // KAWAIITEXTUREFORMAT_HPP
