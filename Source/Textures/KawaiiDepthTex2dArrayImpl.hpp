#ifndef KAWAIIDEPTHTEX2DARRAYIMPL_HPP
#define KAWAIIDEPTHTEX2DARRAYIMPL_HPP

#include "KawaiiTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiDepthTex2dArray.hpp>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiDepthTex2dArrayImpl : public KawaiiTextureImpl
{
  Q_OBJECT

public:
  KawaiiDepthTex2dArrayImpl(KawaiiDepthTex2dArray *model);



  //IMPLEMENT
private:
  KawaiiDepthTex2dArray *model;

  void updateImg();
  void setCompareOp(KawaiiDepthCompareOperation op);
  void onHandleInvalidated();
};

#endif // KAWAIIDEPTHTEX2DARRAYIMPL_HPP
