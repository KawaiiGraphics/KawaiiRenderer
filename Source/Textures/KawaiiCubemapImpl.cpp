#include "KawaiiCubemapImpl.hpp"
#include <cstring>

KawaiiCubemapImpl::KawaiiCubemapImpl(KawaiiCubemap *model):
  KawaiiTextureImpl(model),
  model(model),
  bytesPerPixel(4)
{
  if(model)
    {
      //todo: case when handle is nullptr
      if(getHandle())
        getHandle()->setOnInvalidate(std::bind(&KawaiiCubemapImpl::onHandleInvalidated, this));
      connect(model, &KawaiiCubemap::imageUpdated, this, &KawaiiCubemapImpl::updateImg);
      for(uint16_t i = 0; i < 6; ++i)
        updateImg(i);
    } else
    deleteLater();
}

void KawaiiCubemapImpl::updateImg(quint16 index)
{
  if(getHandle())
    {
      auto fmt = checkTextureFormat();
      const size_t imgBytes = sz.width() * sz.height() * bytesPerPixel;
      bytes.resize(6 * imgBytes);
      std::memcpy(bytes.data()+imgBytes*index, getImg(index).bits(), imgBytes);
      getHandle()->setDataCube(sz.width(), sz.height(), -1, fmt, reinterpret_cast<uint8_t*>(bytes.data()));
    }
}

KawaiiTextureFormat KawaiiCubemapImpl::checkTextureFormat()
{
  for(uint16_t i = 0; i < 6; ++i)
    {
      const auto prevFmt = lastTextureFmt;
      const auto oldBytesPerPixel = bytesPerPixel;
      switch(model->getImage(i).format())
        {
        case QImage::Format_RGB32:
        case QImage::Format_ARGB32:
          rgbImg[i].reset();
          lastTextureFmt = KawaiiTextureFormat::BlueGreenRedAlpha;
          break;

        case QImage::Format_RGBX8888:
        case QImage::Format_RGBA8888:
          rgbImg[i].reset();
          lastTextureFmt = KawaiiTextureFormat::RedGreenBlueAlpha;
          break;

        case QImage::Format_Grayscale8:
          rgbImg[i].reset();
          lastTextureFmt = KawaiiTextureFormat::Red;
          break;

        case QImage::Format_Mono:
        case QImage::Format_MonoLSB:
          rgbImg[i].reset(new QImage(model->getImage(i).convertToFormat(QImage::Format_Grayscale8)));
          lastTextureFmt = KawaiiTextureFormat::Red;
          break;

        default:
          rgbImg[i].reset(new QImage(model->getImage(i).convertToFormat(QImage::Format_RGBA8888)));
          lastTextureFmt = KawaiiTextureFormat::RedGreenBlueAlpha;
          break;
        }
      switch(model->getImage(i).format())
        {
        case QImage::Format_RGB32:
        case QImage::Format_RGBX8888:
        case QImage::Format_RGBA8888:
        case QImage::Format_ARGB32:
          bytesPerPixel = 4;
          break;

        case QImage::Format_Grayscale8:
        case QImage::Format_Mono:
        case QImage::Format_MonoLSB:
          bytesPerPixel = 1;
          break;

        default:
          bytesPerPixel = 4;
          break;
        }

      if(i > 0)
        if(prevFmt != lastTextureFmt
           || sz != model->getImage(i).size()
           || oldBytesPerPixel != bytesPerPixel)
          {
            convertAllSidesToRgba(sz);
            return lastTextureFmt = KawaiiTextureFormat::RedGreenBlueAlpha;
          }
      sz = model->getImage(i).size();
    }
  return lastTextureFmt;
}

void KawaiiCubemapImpl::convertAllSidesToRgba(const QSize &sz)
{
  for(uint16_t i = 0; i < 6; ++i)
    {
      if(sz.width() == 0 || sz.height() == 0 || sz == model->getImage(i).size())
        rgbImg[i].reset(new QImage(model->getImage(i).convertToFormat(QImage::Format_RGBA8888)));
      else
        rgbImg[i].reset(new QImage(model->getImage(i).convertToFormat(QImage::Format_RGBA8888).scaled(sz, Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
    }
}

void KawaiiCubemapImpl::onHandleInvalidated()
{
  KawaiiTextureImpl::onHandleInvalidated();
  const size_t imgBytes = sz.width() * sz.height() * bytesPerPixel;
  bytes.resize(6 * imgBytes);
  for(uint16_t i = 0; i < 6; ++i)
    std::memcpy(bytes.data()+imgBytes*i, getImg(i).bits(), imgBytes);
  getHandle()->setDataCube(sz.width(), sz.height(), -1, lastTextureFmt, reinterpret_cast<uint8_t*>(bytes.data()));
  emit handleChanged();
}

const QImage &KawaiiCubemapImpl::getImg(uint16_t i) const
{
  return rgbImg[i]?
        *rgbImg[i]:
        model->getImage(i);
}
