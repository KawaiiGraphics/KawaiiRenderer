#include "KawaiiTextureImpl.hpp"
#include "../KawaiiRootImpl.hpp"

KawaiiTextureImpl::KawaiiTextureImpl(KawaiiTexture *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  d(nullptr)
{
  if(model)
    {
      connect(model, &KawaiiTexture::minFilterChanged, this, &KawaiiTextureImpl::onMinFilterChanged);
      connect(model, &KawaiiTexture::magFilterChanged, this, &KawaiiTextureImpl::onMagFilterChanged);

      connect(model, &KawaiiTexture::wrapModeSChanged, this, &KawaiiTextureImpl::onWrapModeSChanged);
      connect(model, &KawaiiTexture::wrapModeTChanged, this, &KawaiiTextureImpl::onWrapModeTChanged);
      connect(model, &KawaiiTexture::wrapModeRChanged, this, &KawaiiTextureImpl::onWrapModeRChanged);

      onParentChanged();
    } else
    deleteLater();
}

KawaiiTextureImpl::~KawaiiTextureImpl()
{
  if(d)
    delete d;
}

void KawaiiTextureImpl::onHandleInvalidated()
{
  auto new_handle = root? root->createTexture(model): nullptr;
  auto old_handle = d;

  if(old_handle)
    {
      if(new_handle)
        new_handle->setOnInvalidate(old_handle->getOnInvalidate());

      delete old_handle;
    }

  d = new_handle;
}

void KawaiiTextureImpl::onParentChanged()
{
  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      root = _root;

      auto new_handle = root? root->createTexture(model): nullptr;
      if(d)
        {
          if(new_handle)
            new_handle->setOnInvalidate(d->getOnInvalidate());
          delete d;
        }

      d = new_handle;
    }
}

void KawaiiTextureImpl::onMinFilterChanged(KawaiiTextureFilter filter)
{
  if(d)
    d->setMinFilter(filter);
}

void KawaiiTextureImpl::onMagFilterChanged(KawaiiTextureFilter filter)
{
  if(d)
    d->setMagFilter(filter);
}

void KawaiiTextureImpl::onWrapModeSChanged(KawaiiTextureWrapMode mode)
{
  if(d)
    d->setWrapModeS(mode);
}

void KawaiiTextureImpl::onWrapModeTChanged(KawaiiTextureWrapMode mode)
{
  if(d)
    d->setWrapModeT(mode);
}

void KawaiiTextureImpl::onWrapModeRChanged(KawaiiTextureWrapMode mode)
{
  if(d)
    d->setWrapModeR(mode);
}
