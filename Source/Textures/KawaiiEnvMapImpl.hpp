#ifndef KAWAIIENVMAPIMPL_HPP
#define KAWAIIENVMAPIMPL_HPP

#include <Kawaii3D/Textures/KawaiiEnvMap.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <unordered_map>
#include <QRect>

#include "../Shaders/KawaiiSceneImpl.hpp"
#include "../KawaiiGpuBufImpl.hpp"
#include "../KawaiiRootImpl.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiEnvMapImpl : public KawaiiRendererImpl
{
public:
  explicit KawaiiEnvMapImpl(KawaiiEnvMap *model);
  ~KawaiiEnvMapImpl();

  void drawScene();

protected:
  inline void preDestruct()
  {
    if(root)
      root->removeOffscreenRender(renderI);

    if(drawCache)
      {
        delete drawCache;
        drawCache = nullptr;
      }

    if(hasRenderbuffers)
      {
        deleteRenderbuffers();
        hasRenderbuffers = false;
      }
  }

  inline KawaiiRootImpl *getRoot() const
  { return root; }

  inline KawaiiEnvMap *getModel() const
  { return model; }

  struct DrawCache {
    QRect viewport;
    std::function<void()> drawFunc;
    bool dirty;

    virtual ~DrawCache() = default;
  };

  DrawCache* getCache() const;

  virtual void updateCache(DrawCache &cache, const QRect &viewport) = 0;
  virtual DrawCache* createDrawCache(const QRect &viewport) = 0;

  inline virtual void onRootChanged() {}

  void markCacheDirty();



  //IMPLEMENT
private:
  KawaiiRootImpl::OffscreenRenderIterator renderI;
  std::unordered_map<KawaiiEnvMap::AttachmentMode, QMetaObject::Connection> onAttachmentTexHandleChanged;

  KawaiiEnvMap *model;
  KawaiiRootImpl *root;

  DrawCache* drawCache;
  bool hasRenderbuffers;
  bool hasAttachements;
  bool hasDepthTexture;

  virtual void attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer) = 0;
  virtual void attachDepthRenderbuffer(const QSize &sz) = 0;
  virtual void detachTexture(KawaiiEnvMap::AttachmentMode mode) = 0;
  virtual void deleteRenderbuffers() = 0;
  virtual void setRenderLayer(size_t i) = 0;

  KawaiiGpuBufImpl* sfcUbo(size_t i) const;
  KawaiiGpuBufImpl* camUbo(size_t i) const;
  KawaiiSceneImpl* scene() const;

  void offscrRender();

  void onParentChanged();

  void onTextureAttached(KawaiiEnvMap::AttachmentMode mode, KawaiiTexture *texture, int layer);
  void onTextureDetached(KawaiiEnvMap::AttachmentMode mode);

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // KAWAIIENVMAPIMPL_HPP
