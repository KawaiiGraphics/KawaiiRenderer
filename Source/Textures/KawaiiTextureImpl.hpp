#ifndef KAWAIITEXTUREIMPL_HPP
#define KAWAIITEXTUREIMPL_HPP

#include "KawaiiTextureHandle.hpp"

class KAWAIIRENDERER_SHARED_EXPORT KawaiiTextureImpl: public KawaiiRendererImpl
{
  Q_OBJECT
public:
  KawaiiTextureImpl(KawaiiTexture *model);
  ~KawaiiTextureImpl();

  inline KawaiiTextureHandle *getHandle() const
  { return d; }

  inline auto& getResolution() const
  { return model->getResolution(); }

signals:
  void handleChanged();

protected:
  inline KawaiiRootImpl *getRoot() const
  { return root; }

  void onHandleInvalidated();



  //IMPLEMENT
private:
  KawaiiTexture *model;
  KawaiiRootImpl *root;
  KawaiiTextureHandle *d;

  void onParentChanged();

  void onMinFilterChanged(KawaiiTextureFilter filter);
  void onMagFilterChanged(KawaiiTextureFilter filter);

  void onWrapModeSChanged(KawaiiTextureWrapMode mode);
  void onWrapModeTChanged(KawaiiTextureWrapMode mode);
  void onWrapModeRChanged(KawaiiTextureWrapMode mode);
};

#endif // KAWAIITEXTUREIMPL_HPP
