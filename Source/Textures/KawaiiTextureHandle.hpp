#ifndef KAWAIITEXTUREHANDLE_HPP
#define KAWAIITEXTUREHANDLE_HPP

#include "../KawaiiRenderer_global.hpp"
#include "KawaiiTextureFormat.hpp"

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <Kawaii3D/Textures/KawaiiTextureFilter.hpp>
#include <Kawaii3D/Textures/KawaiiTextureWrapMode.hpp>
#include <Kawaii3D/Textures/KawaiiDepthCompareOperation.hpp>

class KawaiiRootImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiTextureHandle
{
public:
  KawaiiTextureHandle() = default;
  virtual ~KawaiiTextureHandle() = default;

  virtual void setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) = 0;
  virtual void setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr) = 0;

  virtual void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) = 0;
  virtual void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) = 0;

  virtual void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr) = 0;
  virtual void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr) = 0;

  virtual void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) = 0;
  virtual void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) = 0;

  virtual void setCompareOperation(KawaiiDepthCompareOperation op) = 0;

  virtual void setMinFilter(KawaiiTextureFilter filter) = 0;
  virtual void setMagFilter(KawaiiTextureFilter filter) = 0;

  virtual void setWrapModeS(KawaiiTextureWrapMode mode) = 0;
  virtual void setWrapModeT(KawaiiTextureWrapMode mode) = 0;
  virtual void setWrapModeR(KawaiiTextureWrapMode mode) = 0;

  inline void setOnInvalidate(const std::function<void()> &func)
  {
    onInvalidate = func;
  }

  inline const auto &getOnInvalidate() const
  {
    return onInvalidate;
  }

protected:
  inline void invalidate()
  {
    if(onInvalidate)
      onInvalidate();
  }



  //IMPLEMENT
private:
  std::function<void()> onInvalidate;
};

#endif // KAWAIITEXTUREHANDLE_HPP
