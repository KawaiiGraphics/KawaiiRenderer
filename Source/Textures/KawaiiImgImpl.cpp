#include "KawaiiImgImpl.hpp"
#include "KawaiiRootImpl.hpp"
#include "KawaiiTextureHandle.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>

KawaiiImgImpl::KawaiiImgImpl(KawaiiImage *model):
  KawaiiTextureImpl(model),
  model(model)
{
  if(model)
    {
      //todo: case when handle is nullptr
      if(getHandle())
        getHandle()->setOnInvalidate(std::bind(&KawaiiImgImpl::onHandleInvalidated, this));
      connect(model, &KawaiiImage::imageUpdated, this, &KawaiiImgImpl::updateImg);
      updateImg();
    } else
    deleteLater();
}

KawaiiImgImpl::~KawaiiImgImpl()
{
}

void KawaiiImgImpl::updateImg()
{
  if(getHandle())
    {
      auto fmt = checkTextureFormat();
      getHandle()->setData2D(model->getImage().width(), model->getImage().height(), -1, fmt, rgbImg? rgbImg->bits(): model->getImage().bits());
    }
}

KawaiiTextureFormat KawaiiImgImpl::checkTextureFormat()
{
  switch(model->getImage().format())
    {
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
      rgbImg.reset();
      lastTextureFmt = KawaiiTextureFormat::BlueGreenRedAlpha;
      break;

    case QImage::Format_RGBX8888:
    case QImage::Format_RGBA8888:
      rgbImg.reset();
      lastTextureFmt = KawaiiTextureFormat::RedGreenBlueAlpha;
      break;

    case QImage::Format_Grayscale8:
      rgbImg.reset();
      lastTextureFmt = KawaiiTextureFormat::Red;
      break;

    case QImage::Format_Mono:
    case QImage::Format_MonoLSB:
      rgbImg.reset(new QImage(model->getImage().convertToFormat(QImage::Format_Grayscale8)));
      lastTextureFmt = KawaiiTextureFormat::Red;
      break;

    default:
      rgbImg.reset(new QImage(model->getImage().convertToFormat(QImage::Format_RGBA8888)));
      lastTextureFmt = KawaiiTextureFormat::RedGreenBlueAlpha;
      break;
    }
  return lastTextureFmt;
}

void KawaiiImgImpl::onHandleInvalidated()
{
  KawaiiTextureImpl::onHandleInvalidated();
  getHandle()->setData2D(model->getImage().width(), model->getImage().height(), -1, lastTextureFmt, rgbImg? rgbImg->bits(): model->getImage().bits());
  emit handleChanged();
}
