#ifndef KAWAIIFRAMEBUFFERIMPL_HPP
#define KAWAIIFRAMEBUFFERIMPL_HPP

#include "../KawaiiRootImpl.hpp"
#include "KawaiiTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiFramebuffer.hpp>
#include <unordered_map>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiFramebufferImpl: public KawaiiRendererImpl
{
public:
  explicit KawaiiFramebufferImpl(KawaiiFramebuffer *model);
  ~KawaiiFramebufferImpl();

protected:
  inline KawaiiRootImpl* getRoot()
  { return root; }

  inline void preDestruct()
  {
    if(root && renderI.root == root)
      {
        root->removeOffscreenRender(renderI);
        renderI.root = nullptr;
        destroy();
      }
  }

  inline QSize getSize() const
  {
    return QSize(model->getSize().x, model->getSize().y);
  }

  inline KawaiiFramebuffer *getModel() const
  { return model; }



  //IMPLEMENT
private:
  KawaiiRootImpl::OffscreenRenderIterator renderI;
  std::unordered_map<quint32, QMetaObject::Connection> onGbufTexHandleChanged;

  KawaiiFramebuffer *model;
  KawaiiRootImpl *root;

  bool hasAttachements;

  void onParentChanged();
  void offscrRender();

  void onTextureAttached(quint32 gbuf, KawaiiTexture *texture, int layer);
  void onTextureDetached(quint32 gbuf);

  virtual void attachTexture(KawaiiTextureImpl *tex, quint32 gbuf, int layer) = 0;
  virtual void detachTexture(quint32 gbuf) = 0;
  virtual void activate() = 0;
  virtual void deactivate() = 0;
  virtual void destroy() = 0;

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // KAWAIIFRAMEBUFFER_HPP
