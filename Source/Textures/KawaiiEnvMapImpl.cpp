#include "KawaiiEnvMapImpl.hpp"
#include "KawaiiTextureImpl.hpp"

KawaiiEnvMapImpl::KawaiiEnvMapImpl(KawaiiEnvMap *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  drawCache(nullptr),
  hasRenderbuffers(false),
  hasAttachements(false),
  hasDepthTexture(false)
{
  if(model)
    {
      connect(model, &KawaiiEnvMap::parentUpdated, this, &KawaiiEnvMapImpl::onParentChanged);
      connect(model, &KawaiiEnvMap::sceneChanged, this, &KawaiiEnvMapImpl::markCacheDirty);
      connect(model, &KawaiiEnvMap::drawPipelineChanged, this, &KawaiiEnvMapImpl::markCacheDirty);
      connect(model, &KawaiiEnvMap::textureAttached, this, &KawaiiEnvMapImpl::onTextureAttached);
      connect(model, &KawaiiEnvMap::textureDetached, this, &KawaiiEnvMapImpl::onTextureDetached);
      onParentChanged();
    } else
    deleteLater();
}

KawaiiEnvMapImpl::~KawaiiEnvMapImpl()
{
}

void KawaiiEnvMapImpl::drawScene()
{
  if(scene())
    for(size_t i = 0; i < 6; ++i)
      {
        setRenderLayer(i);
        if(camUbo(i) && sfcUbo(i))
          scene()->draw(camUbo(i), sfcUbo(i));
      }
}

KawaiiEnvMapImpl::DrawCache *KawaiiEnvMapImpl::getCache() const
{
  return drawCache;
}

void KawaiiEnvMapImpl::markCacheDirty()
{
  if(drawCache)
    drawCache->dirty = true;
}

KawaiiGpuBufImpl *KawaiiEnvMapImpl::sfcUbo(size_t i) const
{
  return static_cast<KawaiiGpuBufImpl*>(model->getSfcUbo(i)->getRendererImpl());
}

KawaiiGpuBufImpl *KawaiiEnvMapImpl::camUbo(size_t i) const
{
  return static_cast<KawaiiGpuBufImpl*>(model->getCameraUbo(i)->getRendererImpl());
}

KawaiiSceneImpl *KawaiiEnvMapImpl::scene() const
{
  if(model->getScene())
    return static_cast<KawaiiSceneImpl*>(model->getScene()->getRendererImpl());
  else
    return nullptr;
}

void KawaiiEnvMapImpl::offscrRender()
{
  if(!getModel()->enabled || !hasAttachements)
    return;

  getModel()->forallAtachments([] (const KawaiiEnvMap::Attachment &att) {
      if(Q_LIKELY(att.target))
        emit att.target->updated();
    });

  const QRect viewport(QPoint(0,0), QSize(model->getResolution().x, model->getResolution().y));
  if(!drawCache)
    {
      if(auto *cache = createDrawCache(viewport); cache)
        drawCache = cache;
      else
        return;
    }

  if(drawCache->viewport != viewport) {
      drawCache->viewport = viewport;
      drawCache->dirty = true;
    }

  if(drawCache->dirty)
    updateCache(*drawCache, viewport);

  drawCache->drawFunc();
}

void KawaiiEnvMapImpl::onParentChanged()
{
  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      if(root)
        {
          root->removeOffscreenRender(renderI);
          if(hasRenderbuffers)
            deleteRenderbuffers();
        }

      root = _root;
      renderI = root->addOffscreenRender(std::bind(&KawaiiEnvMapImpl::offscrRender, this), nullptr);
      onRootChanged();
    }
}

void KawaiiEnvMapImpl::onTextureAttached(KawaiiEnvMap::AttachmentMode mode, KawaiiTexture *texture, int layer)
{
  if(texture->getRendererImpl())
    {
      hasAttachements = true;
      if(!hasDepthTexture && (mode == KawaiiEnvMap::AttachmentMode::Depth
                              || mode == KawaiiEnvMap::AttachmentMode::DepthStencil))
        hasDepthTexture = true;

      if(hasDepthTexture && hasRenderbuffers)
        {
          deleteRenderbuffers();
          hasRenderbuffers = false;
        }

      auto el = onAttachmentTexHandleChanged.find(mode);
      if(el != onAttachmentTexHandleChanged.end())
        disconnect(el->second);
      else
        el = onAttachmentTexHandleChanged.insert({mode, {}}).first;

      auto tex_impl = static_cast<KawaiiTextureImpl*>(texture->getRendererImpl());
      el->second = connect(tex_impl, &KawaiiTextureImpl::handleChanged, this, [this, tex_impl, mode, layer] {
          attachTexture(tex_impl, mode, layer);
        });
      attachTexture(tex_impl, mode, layer);
    }
}

void KawaiiEnvMapImpl::onTextureDetached(KawaiiEnvMap::AttachmentMode mode)
{
  auto el = onAttachmentTexHandleChanged.find(mode);
  if(el != onAttachmentTexHandleChanged.end())
    disconnect(el->second);

  detachTexture(mode);
  hasAttachements = false;
  if(hasDepthTexture && (mode == KawaiiEnvMap::AttachmentMode::Depth
                  || mode == KawaiiEnvMap::AttachmentMode::DepthStencil))
    hasDepthTexture = false;

  model->forallAtachments([this] (const KawaiiEnvMap::Attachment &att) {
      if(att.target->getRendererImpl())
        {
          hasAttachements = true;
          if(!hasDepthTexture && (att.mode == KawaiiEnvMap::AttachmentMode::Depth
                           || att.mode == KawaiiEnvMap::AttachmentMode::DepthStencil))
            hasDepthTexture = true;
        }
    });

  if(hasAttachements && !hasDepthTexture && !hasRenderbuffers)
    {
      attachDepthRenderbuffer(QSize(model->getResolution().x, model->getResolution().y));
      hasRenderbuffers = true;
    }
}

void KawaiiEnvMapImpl::initConnections()
{
  model->forallAtachments([this] (const KawaiiEnvMap::Attachment &att) {
      onTextureAttached(att.mode, att.target.data(), att.layer);
    });

  if(hasAttachements && !hasDepthTexture)
    {
      attachDepthRenderbuffer(QSize(model->getResolution().x, model->getResolution().y));
      hasRenderbuffers = true;
    }
}
