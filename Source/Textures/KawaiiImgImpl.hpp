#ifndef KAWAIIIMGIMPL_HPP
#define KAWAIIIMGIMPL_HPP

#include "KawaiiTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiImage.hpp>

class KawaiiRootImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiImgImpl : public KawaiiTextureImpl
{
  Q_OBJECT

public:
  explicit KawaiiImgImpl(KawaiiImage *model);
  ~KawaiiImgImpl();



  //IMPLEMENT
private:
  KawaiiImage *model;
  std::unique_ptr<QImage> rgbImg;
  KawaiiTextureFormat lastTextureFmt;

  void updateImg();

  KawaiiTextureFormat checkTextureFormat();

  void onHandleInvalidated();
};

#endif // KAWAIIIMGIMPL_HPP
