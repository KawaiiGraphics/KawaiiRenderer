#include "KawaiiDepthCubemapArrayImpl.hpp"

KawaiiDepthCubemapArrayImpl::KawaiiDepthCubemapArrayImpl(KawaiiDepthCubemapArray *model):
  KawaiiTextureImpl(model),
  model(model)
{
  if(model)
    {
      if(getHandle())
        getHandle()->setOnInvalidate(std::bind(&KawaiiDepthCubemapArrayImpl::onHandleInvalidated, this));

      connect(model, &KawaiiDepthCubemapArray::compareOperationChanged, this, &KawaiiDepthCubemapArrayImpl::setCompareOp);
      connect(model, &KawaiiDepthCubemapArray::layersChanged, this, &KawaiiDepthCubemapArrayImpl::updateImg);
      connect(model, &KawaiiDepthCubemapArray::resized, this, &KawaiiDepthCubemapArrayImpl::updateImg);

      updateImg();
      setCompareOp(model->getCompareOperation());
    } else
    deleteLater();
}

void KawaiiDepthCubemapArrayImpl::updateImg()
{
  const QSize &sz = model->getSize();
  getHandle()->setDataCube(sz.width(), sz.height(), model->getLayers(), KawaiiTextureFormat::Depth, static_cast<const float*>(nullptr));
}

void KawaiiDepthCubemapArrayImpl::setCompareOp(KawaiiDepthCompareOperation op)
{
  getHandle()->setCompareOperation(op);
}

void KawaiiDepthCubemapArrayImpl::onHandleInvalidated()
{
  KawaiiTextureImpl::onHandleInvalidated();
  updateImg();
  setCompareOp(model->getCompareOperation());
  emit handleChanged();
}
