#ifndef KAWAIIDEPTHCUBEMAPARRAYIMPL_HPP
#define KAWAIIDEPTHCUBEMAPARRAYIMPL_HPP

#include "KawaiiTextureImpl.hpp"
#include <Kawaii3D/Textures/KawaiiDepthCubemapArray.hpp>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiDepthCubemapArrayImpl: public KawaiiTextureImpl
{
  Q_OBJECT

public:
  KawaiiDepthCubemapArrayImpl(KawaiiDepthCubemapArray *model);



  // IMPLEMENT
private:
  KawaiiDepthCubemapArray *model;

  void updateImg();
  void setCompareOp(KawaiiDepthCompareOperation op);
  void onHandleInvalidated();
};

#endif // KAWAIIDEPTHCUBEMAPARRAYIMPL_HPP
