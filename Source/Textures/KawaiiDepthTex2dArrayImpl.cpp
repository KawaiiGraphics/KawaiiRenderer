#include "KawaiiDepthTex2dArrayImpl.hpp"

KawaiiDepthTex2dArrayImpl::KawaiiDepthTex2dArrayImpl(KawaiiDepthTex2dArray *model):
  KawaiiTextureImpl(model),
  model(model)
{
  if(model)
    {
      if(getHandle())
        getHandle()->setOnInvalidate(std::bind(&KawaiiDepthTex2dArrayImpl::onHandleInvalidated, this));

      connect(model, &KawaiiDepthTex2dArray::compareOperationChanged, this, &KawaiiDepthTex2dArrayImpl::setCompareOp);
      connect(model, &KawaiiDepthTex2dArray::layersChanged, this, &KawaiiDepthTex2dArrayImpl::updateImg);
      connect(model, &KawaiiDepthTex2dArray::resized, this, &KawaiiDepthTex2dArrayImpl::updateImg);

      updateImg();
      setCompareOp(model->getCompareOperation());
    } else
    deleteLater();
}

void KawaiiDepthTex2dArrayImpl::updateImg()
{
  const QSize &sz = model->getSize();
  getHandle()->setData2D(sz.width(), sz.height(), model->getLayers(), KawaiiTextureFormat::Depth, static_cast<const float*>(nullptr));
}

void KawaiiDepthTex2dArrayImpl::setCompareOp(KawaiiDepthCompareOperation op)
{
  getHandle()->setCompareOperation(op);
}

void KawaiiDepthTex2dArrayImpl::onHandleInvalidated()
{
  KawaiiTextureImpl::onHandleInvalidated();
  updateImg();
  setCompareOp(model->getCompareOperation());
  emit handleChanged();
}
