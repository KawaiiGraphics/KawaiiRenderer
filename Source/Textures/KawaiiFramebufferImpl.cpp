#include "KawaiiFramebufferImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include "Renderpass/KawaiiRenderpassImpl.hpp"
#include "KawaiiTextureHandle.hpp"
#include "KawaiiRootImpl.hpp"

KawaiiFramebufferImpl::KawaiiFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiRendererImpl(model),
  model(model),
  root(nullptr),
  hasAttachements(false)
{
  if(model)
    {
      connect(model, &KawaiiFramebuffer::parentUpdated, this, &KawaiiFramebufferImpl::onParentChanged);
      connect(model, &KawaiiFramebuffer::textureAttached, this, &KawaiiFramebufferImpl::onTextureAttached);
      connect(model, &KawaiiFramebuffer::textureDetached, this, &KawaiiFramebufferImpl::onTextureDetached);
      onParentChanged();
    } else
    deleteLater();
}

KawaiiFramebufferImpl::~KawaiiFramebufferImpl()
{
  Q_ASSERT(renderI.root == nullptr);
}

void KawaiiFramebufferImpl::onParentChanged()
{
  auto _root = KawaiiRootImpl::findRootImpl(this);
  if(_root != root)
    {
      if(root)
        {
          root->removeOffscreenRender(renderI);
          destroy();
        }

      root = _root;
      renderI = root->addOffscreenRender(std::bind(&KawaiiFramebufferImpl::offscrRender, this), getModel());
    }
}

void KawaiiFramebufferImpl::offscrRender()
{
  if(!getModel()->renderingEnabled
     || !getModel()->getRenderpass()
     || !hasAttachements
     || !getModel()->getRenderpass()->isRedrawNeeded()) return;

  auto renderpass = static_cast<KawaiiRenderpassImpl*>(
        getModel()->getRenderpass()->getRendererImpl());

  if(Q_UNLIKELY(!renderpass))
    return;

  getModel()->forallAtachments([] (const KawaiiFramebuffer::Attachment &att) {
      if(Q_LIKELY(att.target))
        emit att.target->updated();
    });

  activate();
  renderpass->draw();
  deactivate();
}

void KawaiiFramebufferImpl::onTextureAttached(quint32 gbuf, KawaiiTexture *texture, int layer)
{
  if(texture->getRendererImpl())
    {
      hasAttachements = true;
      auto el = onGbufTexHandleChanged.find(gbuf);
      if(el != onGbufTexHandleChanged.end())
        disconnect(el->second);
      else
        el = onGbufTexHandleChanged.insert({gbuf, {}}).first;
      auto *tex_impl = static_cast<KawaiiTextureImpl*>(texture->getRendererImpl());
      el->second = connect(tex_impl, &KawaiiTextureImpl::handleChanged, this, [this, tex_impl, gbuf, layer] {
          attachTexture(tex_impl, gbuf, layer);
        });
      attachTexture(tex_impl, gbuf, layer);
    }
}

void KawaiiFramebufferImpl::onTextureDetached(quint32 gbuf)
{
  auto el = onGbufTexHandleChanged.find(gbuf);
  if(el != onGbufTexHandleChanged.end())
    disconnect(el->second);

  detachTexture(gbuf);
  hasAttachements = false;

  model->forallAtachments([this] (const KawaiiFramebuffer::Attachment &att) {
      if(att.target->getRendererImpl())
        {
          hasAttachements = true;
        }
    });
}

void KawaiiFramebufferImpl::initConnections()
{
  model->forallAtachments([this] (const KawaiiFramebuffer::Attachment &att) {
      onTextureAttached(att.useGbuf, att.target.data(), att.layer);
    });
}
