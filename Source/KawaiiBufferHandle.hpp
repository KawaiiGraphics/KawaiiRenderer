#ifndef KAWAIIBUFFERHANDLE_HPP
#define KAWAIIBUFFERHANDLE_HPP

#include <Kawaii3D/KawaiiBufferTarget.hpp>
#include "KawaiiRenderer_global.hpp"

#include <memory>
#include <QObject>
#include <functional>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiBufferHandle
{
public:
  KawaiiBufferHandle() = default;
  virtual ~KawaiiBufferHandle() = default;

  virtual void setBufferData(const void *ptr, size_t n) = 0;
  virtual void sendChunkToGpu(size_t offset, size_t n) = 0;
  virtual size_t getBufferSize() const = 0;

  virtual void bind(KawaiiBufferTarget target) = 0;
  virtual void unbind(KawaiiBufferTarget target) = 0;
  virtual void bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint) = 0;
  virtual void userBinding(KawaiiBufferTarget target, uint32_t bindingPoint) = 0;
  virtual void unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint) = 0;

  template<typename T>
  inline void setData(const std::vector<T> &vec)
  { setBufferData(vec.data(), sizeof(T)*vec.size()); }

  template<typename T>
  inline void updateDataElements(size_t first, size_t count)
  {
    sendChunkToGpu(first * sizeof(T),  count * sizeof(T));
  }

  template<typename T>
  inline size_t getElemetsCount() const
  {
    return getBufferSize() / sizeof(T);
  }

  void setOnInvalidate(const std::function<void()> &func);

  static void replace(std::unique_ptr<KawaiiBufferHandle> &oldHandle, KawaiiBufferHandle *newHandle, const std::function<void()> &onInvalidate);
  static void replace(KawaiiBufferHandle *&oldHandle, KawaiiBufferHandle *newHandle, const std::function<void()> &onInvalidate);

protected:
  void invalidate();



  //IMPLEMENT
private:
  std::function<void()> onInvalidate;
};

#endif // KAWAIIBUFFERHANDLE_HPP
