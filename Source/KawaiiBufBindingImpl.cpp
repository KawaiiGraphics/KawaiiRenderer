#include "KawaiiBufBindingImpl.hpp"
#include <Kawaii3D/KawaiiRoot.hpp>
#include "KawaiiRootImpl.hpp"

KawaiiBufBindingImpl::KawaiiBufBindingImpl(KawaiiBufBinding *model):
  KawaiiRendererImpl(model),
  buf(nullptr),
  model(model),
  bindingPoint(model->getBindingPoint())
{
  if(model)
    {
      connect(model, &KawaiiBufBinding::bufferChanged, this, &KawaiiBufBindingImpl::onBufferChanged);
      connect(model, &KawaiiBufBinding::bindingPointChanged, this, &KawaiiBufBindingImpl::onBindingPointChanged);

      if(model->getBuf())
        buf = static_cast<KawaiiGpuBufImpl*>(model->getBuf()->getRendererImpl());

//      bind();
    } else
    deleteLater();
}

KawaiiBufBindingImpl::~KawaiiBufBindingImpl()
{
}

void KawaiiBufBindingImpl::bind()
{
  if(buf)
    {
      connect(buf, &KawaiiGpuBufImpl::handleChanged, this, &KawaiiBufBindingImpl::bind, Qt::UniqueConnection);
      buf->userBinding(model->getTarget(), bindingPoint);
    }
}

void KawaiiBufBindingImpl::onBindingPointChanged(uint32_t binding)
{
  if(buf)
    {
      buf->unbindFromBlock(model->getTarget(), bindingPoint);
      buf->userBinding(model->getTarget(), binding);
    }
  bindingPoint = binding;
}

void KawaiiBufBindingImpl::onBufferChanged(KawaiiGpuBuf *target)
{
  if(buf)
    {
      disconnect(buf, &KawaiiGpuBufImpl::handleChanged, this, &KawaiiBufBindingImpl::bind);
      buf->unbindFromBlock(model->getTarget(), bindingPoint);
    }
  if(target)
    {
      buf = static_cast<KawaiiGpuBufImpl*>(target->getRendererImpl());
      bind();
    }
}

void KawaiiBufBindingImpl::initConnections()
{
  if(model->getBuf())
    {
      buf = static_cast<KawaiiGpuBufImpl*>(model->getBuf()->getRendererImpl());
      bind();
    }
}
