#ifndef KAWAIICAMERAIMPL_HPP
#define KAWAIICAMERAIMPL_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/KawaiiCamera.hpp>
#include "KawaiiRenderer_global.hpp"
#include "KawaiiGpuBufImpl.hpp"
#include <unordered_map>
#include <QRect>

class KawaiiSceneImpl;

class KAWAIIRENDERER_SHARED_EXPORT KawaiiCameraImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiCameraImpl(KawaiiCamera *model);
  ~KawaiiCameraImpl();

  void draw(KawaiiGpuBufImpl *sfcUbo);

  KawaiiCamera *getModel() const;
  glm::mat4 getViewProjectionMat(const glm::mat4 &projMat) const;



  //IMPLEMENT
private:
  KawaiiCamera *model;

  KawaiiGpuBufImpl* ubo() const;
  KawaiiSceneImpl* scene() const;
};

#endif // KAWAIICAMERAIMPL_HPP
