#include "KawaiiSurfaceImpl.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include "Renderpass/KawaiiRenderpassImpl.hpp"
#include "KawaiiRootImpl.hpp"

KawaiiSurfaceImpl::KawaiiSurfaceImpl(KawaiiSurface *model):
  KawaiiRendererImpl(model),
  model(model)
{
  if(model)
    {
      model->setRenderFunc(std::bind(&KawaiiSurfaceImpl::render, this));
      connect(model, &KawaiiSurface::resized, this, &KawaiiSurfaceImpl::resize);
      connect(&model->getWindow(), &QWindow::visibleChanged, this, &KawaiiSurfaceImpl::onVisibleChanged);
    } else
    deleteLater();
}

KawaiiSurfaceImpl::~KawaiiSurfaceImpl()
{
  if(model)
    model->detachRenderFuncs();
}

void KawaiiSurfaceImpl::render()
{
  if(Q_UNLIKELY(!model->getRoot()))
    return;

  auto root = static_cast<KawaiiRootImpl*>(model->getRoot()->getRendererImpl());
  if(Q_UNLIKELY(!root))
    return;

  if(!startRendering())
    return;

  root->drawOffscreen();
  for(size_t i = 0; i < model->tileCount(); ++i)
    {
      auto &tile = model->getTile(i);
      if(tile.renderingEnabled && tile.getRenderpass())
        static_cast<KawaiiRenderpassImpl*>(tile.getRenderpass()->getRendererImpl())->draw();
    }
  finishRendering();
}

void KawaiiSurfaceImpl::onVisibleChanged(bool vis)
{
  if(!vis)
    invalidate();
}
