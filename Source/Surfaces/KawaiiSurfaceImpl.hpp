#ifndef KAWAIISURFACEIMPL_HPP
#define KAWAIISURFACEIMPL_HPP

#include "../KawaiiRenderer_global.hpp"

#include <Kawaii3D/Surfaces/KawaiiSurface.hpp>
#include <Kawaii3D/KawaiiRendererImpl.hpp>

class KAWAIIRENDERER_SHARED_EXPORT KawaiiSurfaceImpl: public KawaiiRendererImpl
{
  Q_OBJECT

public:
  KawaiiSurfaceImpl(KawaiiSurface *model);
  ~KawaiiSurfaceImpl();

protected:
  inline KawaiiSurface *getModel() const
  { return model; }



  //IMPLEMENT
private:
  KawaiiSurface *model;

  virtual void resize(const QSize &sz) = 0;

  virtual void invalidate() = 0;

  virtual bool startRendering() = 0;
  virtual void finishRendering() = 0;

  void render();

  void onVisibleChanged(bool vis);
};

#endif // KAWAIISURFACEIMPL_HPP
