#include <qglobal.h>

#ifdef Q_CC_MINGW

extern "C" Q_DECL_HIDDEN void __cxa_pure_virtual()
{ Q_UNREACHABLE(); }

#endif
